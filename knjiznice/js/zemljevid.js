/* global L, distance */  

var mapa;

window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [46.0536368, 14.5191651],
    zoom: 14
    // maxZoom: 3
  };

   // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);
  
  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    var eLat = latlng.lat;
    var eLng = latlng.lng;
    pridobiPodatke("vrsta", function(jsonRezultat) {
       izrisRezultatov(jsonRezultat, 1, eLat, eLng); 
    });
  }
  
  mapa.on('click', obKlikuNaMapo);
  
  dodajBolnisnice();
  
});

/**
 * Na zemljevid dodaj oznake z bližnjimi bolnisnicami
 */
function dodajBolnisnice() {
  pridobiPodatke("vrsta", function (jsonRezultat) {
    izrisRezultatov(jsonRezultat, 0, 0, 0);
  });
}

/**
 * Dostopaj do JSON datoteke in vsebino JSON datoteke vrni v povratnem klicu
 * 
 * @param callback povratni klic z vsebino zahtevane JSON datoteke
 */
function pridobiPodatke(vrsta, callback) {
  
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        // vrnemo rezultat
        callback(json);
    }
  };
  xobj.send(null);
}

/**
 * Na podlagi podanih interesnih točk v GeoJSON obliki izriši
 * posamezne točke na zemljevid
 * 
 * @param jsonRezultat interesne točke v GeoJSON obliki
 */
function izrisRezultatov(jsonRezultat, barvanje, eLat, eLng) {
  var znacilnosti = jsonRezultat.features;
  var coord = [] ;
  var barva = 'blue';
  
  for (var i = 0; i < znacilnosti.length; i++) {
    barva = 'blue';
    var ime = znacilnosti[i].properties.name;
    var ulica = znacilnosti[i].properties["addr:street"] ;
    var hisna = znacilnosti[i].properties["addr:housenumber"] ;
    var namen = znacilnosti[i].properties.amenity ;
    var id = znacilnosti[i].properties["@id"] ;

    // pridobimo koordinate
    if(znacilnosti[i].geometry.type == "Polygon" || znacilnosti[i].geometry.type == "LineString") {
      
      coord = znacilnosti[i].geometry.coordinates ;
      
      for(var k = 0; k < znacilnosti[i].geometry.coordinates.length; k++) {
        for (var j = 0; j < znacilnosti[i].geometry.coordinates[k].length; j++) {
          var x = coord[k][j][0];
          coord[k][j][0] = coord[k][j][1];
          coord[k][j][1] = x;
        }
      }
      
      if (barvanje == 1 && prikaziOznako(coord[0][0][0], coord[0][0][1], eLat, eLng) == true) {
        barva = 'green';
      }
    
      if (ime != undefined && ulica != undefined)
      var polygon = L.polygon(coord, {color: barva}).bindPopup("<div>Naziv: " + ime + "</div>" + "<div>Ulica: " + ulica + " " + hisna + "</div>");
      
      else if (ime != undefined)
      polygon = L.polygon(coord, {color: barva}).bindPopup("<div>Naziv: " + ime + "</div>" + "<div>Namen: " + namen + "</div>");
      
      else if (ulica != undefined)
      polygon = L.polygon(coord, {color: barva}).bindPopup("<div>Ulica: " + ulica + " " + hisna + "</div>" + "<div>Namen: " + namen + "</div>");
      
      else
      polygon = L.polygon(coord, {color: barva}).bindPopup("<div>Namen: " + namen + "</div>" + "<div>Id: " + id + "</div>");
      
      polygon.addTo(mapa);
    }
    else {
      var lat = znacilnosti[i].geometry.coordinates[0];
      var lan = znacilnosti[i].geometry.coordinates[1];
      
      if (barvanje == 1 && prikaziOznako(lan, lat, eLat, eLng) == true) {
        barva = 'green';
      }
      
      var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-' + barva + '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  var marker = L.marker([lan, lat], {icon: ikona});
  
  // sporocilo oblacka
  if (ulica != undefined)
  marker.bindPopup("<div>Naziv: " + ime + "</div>" + "<div> Ulica: " + ulica + " " + hisna + "</div>").openPopup();
  
  else if (ime != undefined)
  marker.bindPopup("<div>Naziv: " + ime + "</div>" + "<div>Namen:  " + namen + "</div>").openPopup();
  
  else
  marker.bindPopup("<div>Namen:  " + namen + "</div>" + "<div>Id: " + id + "</div>").openPopup();

  // Dodamo točko na mapo
  marker.addTo(mapa);
    }
  }
  barva = 'blue';
}

function prikaziOznako(lat, lng, eLat, eLng) {
  var razdalja = 5;
  if (distance(lat, lng, eLat, eLng, "K") >= razdalja) return false;
  else return true;
}