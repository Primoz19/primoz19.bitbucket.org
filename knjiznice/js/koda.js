var a;
var b;
var c;

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  a = 0 ;
  b = 0 ;
  c = 0 ;
  // ehrId = "";
  pacient1();
  kreirajEHR();
  pacient2();
  kreirajEHR();
  pacient3();
  kreirajEHR();

  // TODO: Potrebno implementirati
}

function pacient1() {
  document.getElementById("ime").value = "Janez";
  document.getElementById("priimek").value = "Novak";
  document.getElementById("rojstni").value = "1985-02-03";
}

function pacient2() {
  document.getElementById("ime").value = "Ana";
  document.getElementById("priimek").value = "Dolenc";
  document.getElementById("rojstni").value = "1978-10-08";
}

function pacient3() {
  document.getElementById("ime").value = "Matjaž";
  document.getElementById("priimek").value = "Horvat";
  document.getElementById("rojstni").value = "1993-04-12";
}

function kreirajEHR() {
	var ime = $("#ime").val();
	var priimek = $("#priimek").val();
  var datumRojstva = $("#rojstni").val();

	if (!ime || !priimek || !datumRojstva) {
		$("#sporociloKreiranje").html("<span>Nobeno mesto ne sme biti prazno!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#sporociloKreiranje").html("Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#ehr").val(ehrId);
              if (a == 0) { 
                $("#noviPacienti1").html("<span>" + "EHR prvega pacienta je:" + ehrId + "</span>");
                a = 1;
              }
              else if (b == 0) { 
                $("#noviPacienti2").html("<span>" + "EHR drugega pacienta je:" + ehrId + "</span>");
                b = 1;
              }
              else if (c==0) { 
                $("#noviPacienti3").html("<span>" + "EHR tretjega pacienta je:" + ehrId + "</span>");
                c = 1;
              }
            }
          },
        });
      }
		});
	}
}

function preberiEHR() {
	var ehrId = $("#ehr").val();

	if (!ehrId) {
		$("#sporociloBranje").html("<span>To mesto ne sme biti prazno!</span>");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#sporociloBranje").html("<span>" + "Osebi s tem EHR id-jem je ime " + party.firstNames + " " +
          party.lastNames + "." +
          "</span>");
  		},
		});
	}
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
